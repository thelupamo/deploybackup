export class Todo {
    _id: string;
    title: string;
    task: string;
    completed: boolean;
}
